# Welcome to Astro + MDX + Tailwind

Create the project

```shell
npm create astro@latest
```

Deploy to GitLab pages

```shell
mv public static
```

```javascript
// astro.config.mjs
import {
    defineConfig
} from 'astro/config';

export default defineConfig({
    site: 'https://jameskolean.gitlab.io',
    base: import.meta.env.MODE === 'development' ? '/' : '/astro-mdx-tailwind',
    // GitLab Pages requires exposed files to be located in a folder called "public".
    // So we're instructing Astro to put the static build output in a folder of that name.
    outDir: 'public',
    // The folder name Astro uses for static files (`public`) is already reserved
    // for the build output. So in deviation from the defaults we're using a folder
    // called `static` instead.
    publicDir: 'static',
});
```

> ***Note:*** Change `site` to your own site and change `base` to your own base.

```yaml
# .gitlab-ci.yml
# This folder is cached between builds
# https://docs.gitlab.com/ce/ci/yaml/README.html#cache
cache:
  paths:
    - node_modules/
    # Enables git-lab CI caching. Both .cache and public must be cached, otherwise builds will fail.
    - .cache/
    - public/

pages:
  script:
    - npm install
    - npm run build
  artifacts:
    paths:
      - public
  only:
    - main

```

## 🧞 Commands

All commands are run from the root of the project, from a terminal:

| Command                | Action                                             |
| :--------------------- | :------------------------------------------------- |
| `npm install` | Installs dependencies                              |
| `npm run dev` | Starts local dev server at `localhost:3000` |
| `npm run build` | Build your production site to `./public/` |
| `npm run preview` | Preview your build locally, before deploying       |
| `npm run astro ...` | Run CLI commands like `astro add` , `astro preview` |
| `npm run astro --help` | Get help using the Astro CLI                       |
