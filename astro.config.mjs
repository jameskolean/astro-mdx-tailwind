import { defineConfig } from 'astro/config';
import mdx from '@astrojs/mdx';
import preact from "@astrojs/preact";
import tailwind from "@astrojs/tailwind";
// const MODE = import.meta.env.MODE;

// https://astro.build/config
export default defineConfig({
  site: 'https://jameskolean.gitlab.io',
  // Mode not working anymore
  // base: MODE === 'development' ? '/' : '/astro-mdx-tailwind',
  base: process.env.PROD ? '/astro-mdx-tailwind' : '/',
  // GitLab Pages requires exposed files to be located in a folder called "public".
  // So we're instructing Astro to put the static build output in a folder of that name.
  outDir: 'public',
  // The folder name Astro uses for static files (`public`) is already reserved
  // for the build output. So in deviation from the defaults we're using a folder
  // called `static` instead.
  publicDir: 'static',
  markdown: {
    drafts: true
  },
  integrations: [mdx({
    drafts: true
  }), preact({
    compat: true
  }), tailwind()]
});