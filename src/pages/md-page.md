---

# src/pages/md-page.md

layout: '../layouts/LayoutMarkdown.astro'
title: Markdown Example
---

# Markdown

This Markdown file creates a page at `your-domain.com/md-page/`

It probably isn't styled much, but Markdown does support:
* **bold** and _italics._
* lists
* [links](https://astro.build)
* and more!
